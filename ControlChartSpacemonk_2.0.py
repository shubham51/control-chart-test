import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
import re
import openpyxl
import xlsxwriter
import math

#---------------------------Plotting and report generation-------------------------#
def report_generator(data_list, cond_list, limits, month, year, s):
    try:
        if((s=="week" and len(data_list)<7) or (s=="month" and len(data_list)<1)):
            print("Plot cannot be made. Insufficient data points have been entered")
            return
        if(s == "week"):
            l = len(data_list)
            data_prev_7days = data_list[-7:]
            control_conditions_prev_7days = cond_list[-7:]
            mean=chartPlot(data_prev_7days,control_conditions_prev_7days, limits, month, year,l-6)
            print("Average Consumption for last 7 days : " + str(round(mean,3)))
            for i in range(0,7):
                if(control_conditions_prev_7days[i] == 1):
                    print( str(l-6+i) + getMonth(month) + ", " + str(year) + " had an out of control point, user decided not to use out of control point for control chart." )
                elif(control_conditions_prev_7days[i] == 2):
                    print( str(l-6+i) + getMonth(month) + ", " + str(year) +" had an out of control point, user decided to use out of control point for control chart" )
            return
            
        if(s == "month"):
            l = len(data_list)
            mean=chartPlot(data_list,cond_list, limits, month, year,1)
            print("Average Consumption for last month : " + str(round(mean,3)))
            for i in range(0,l):
                if(cond_list[i] == 1):
                    print( str(i+1) + getMonth(month) + ", " + str(year) + " had an out of control point, user decided not to use out of control point for control chart." )
                elif(cond_list[i] == 2):
                    print( str(i+1) + getMonth(month) + ", " + str(year) +" had an out of control point, user decided to use out of control point for control chart" )
    except:
        print("Plot cannot be made")


def chartPlot(data_list, cond_list, limits, month, year, start_date ):
    data_len = len(data_list)
    dates = []
    counter=0
    summ=0
    for i in range(start_date,start_date+data_len):
        dates.append(str(i) + " " + getMonth(month) + ", " + str(year))
    for i in range(0,data_len):
        if(cond_list[i]==-1):
            plt.scatter(dates[i],None)
        elif(cond_list[i]==1):
            plt.scatter(dates[i],-data_list[i],c='b')
            summ=summ-data_list[i]
            counter=counter+1
        else:
            summ=summ+data_list[i]
            counter=counter+1
            plt.scatter(dates[i],data_list[i],c = 'b')
            
    plt.gca().update(dict(title='Control Chart for Last ' + str(data_len) + ' Days', xlabel='Dates', ylabel='Consumption'))
    for j in range(0,len(limits)):             
            if(len(limits) == 7):
                if(j==0 or j==3 or j==6):
                    plt.axhline(y=limits[j], color='red', linestyle='-')
                    continue
                plt.axhline(y=limits[j], color='black', linestyle='-') 
            if(len(limits) == 5):
                if(j==0 or j==2 or j==4):
                    plt.axhline(y=limits[j], color='red', linestyle='-')
                    continue    
                plt.axhline(y=limits[j], color='black', linestyle='-')   
    col=['red','blue','green','yellow','orange','black','purple']
    for i in range(0,len(limits)-1):
        a1=[limits[i]]*len(dates)
        a2=[limits[i+1]]*len(dates)
        plt.fill_between(dates,a1,a2,alpha=0.30, color=col[i],interpolate=True,label='Zone '+str(i+1))
    plt.legend()
    plt.xticks(rotation= 90)
    plt.show()
    return summ/counter


#---------------------------Exports data to excel file-------------------------#
def list_to_excel(d,month,year,date = 1):
    try:
        wbkName = 'Control_Chart_Training.xlsx'
        wbk = openpyxl.load_workbook(wbkName)
        df = pd.read_excel('Control_Chart_Training.xlsx', sheet_name='Sheet1')
        months = [] 
        for col in df.columns: 
            months.append(int(col))
        year_month = month + 100*year
        if(year_month in months):
            column_id = months.index(year_month)+1
        else:
            column_id = len(df.iloc[0]) + 1
        days = len(d)
        for wks in wbk.worksheets:
            #------Searching next empty column------#
            wks.cell(row = 1, column = column_id).value = year*100+month
            for myRow in range(0,days):
               wks.cell(row=myRow+date+1, column=column_id).value = d[myRow]
        wbk.save(wbkName)
        wbk.close
    except:
        data = []
        days = len(d)
        for i in range(0,days):
            data.append(d[i])
        print(data)
        filename = 'Control_Chart_Training.xlsx'
        with xlsxwriter.Workbook(filename) as workbook:
            worksheet = workbook.add_worksheet()
            worksheet.write(0,0,year*100+month)
            for row in range(0,days):
                worksheet.write(row+date,0,data[row])
                
                
#-------------------Displays rules and asks user for input---------------------#
def rule_selection():
    print("Rule 1 : If any new data point is beyond Upper Control Limit or Lower Control Limit, you will be prompted to decide to include or exclude data point.")
    print("\nRule 2 : If any 2 out of 3 consecutive points lie near Upper Control Limit, you will be prompted to decide to include or exclude data point.")
    print("\nRules 1 and 2 are always implemented. Choose from the rules listed below (note that the below rules are not applicable if risk coefficient is set to 2):")
    print("\nRule 3 : If any 4 out of 5 consecutive points lie in Zone 2 or 5, system advises not to include.")
    print("\nRule 4 : If 7 consecutive points lie in zones near Central Limit , Control Chart is not built afterwards")
    print("\nEnter 0 if no rules to be included, 1 if Rule 3 to be included, 2 if Rule 4 to be included, 3 if both to be included: ")


#----------------Checks how many days are filled for input month----------------#
def reader(month,year):
    df = pd.read_excel('Control_Chart_Training.xlsx', sheet_name='Sheet1',nrows=50)
    count=0
    year_month=100*year+month    
    for col in df.columns: 
        if(int(col)==(year_month)):
                X = df[year_month].values.tolist()
                X = [x for x in X if (math.isnan(x) == False and x>=0)]
                count=len(X)
    return count 

#----------------Basic functions for month----------------#
def daysInMonth(month,year):
    if(year%4==0 and month==2):
        return 29
    days = [31,28,31,30,31,30,31,31,30,31,30,31]    
    return days[month-1]

def getMonth(month):
    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    return months[(month-1)%12]
  
    
#----------------Selecting month from which data for rolling window is chosen----------------#
def month_selector(m,y):
    df = pd.read_excel('Control_Chart_Training.xlsx', sheet_name='Sheet1')    
    months = []
    for col in df.columns: 
        months.append(int(col))
    months.sort()
    month_suggest=0
    year_suggest=0
    for i in range(len(months)):
        year = months[i]//100
        month = months[i] - year*100
        s = getMonth(month) + ", "+str(year)
        if(((year>year_suggest) or (month>month_suggest and year==year_suggest)) and (month!=m or year!=y)):
            year_suggest=year
            month_suggest=month
        print(s)
    print("Use data from "+getMonth(month_suggest)+", "+str(year_suggest)+" for most appropriate results")    
    [month_selected,year_selected] = input("Enter month selected in MM YYYY format: ").split()
    month_selected = int(month_selected)
    year_selected = int(year_selected)
    return (month_selected,year_selected)


#----------------30 days dataset----------------#
def rolling_dataset(m,y,filled_days):
    df = pd.read_excel('Control_Chart_Training.xlsx', sheet_name='Sheet1',nrows=50)
    (ms,ys)=month_selector(m,y)
    Y = df[ys*100+ms].values.tolist()
    dates = []
    for i in range (0,daysInMonth(ms,ys)):
        if(math.isnan(Y[i]) == False and Y[i]>=0):
            dates.append(i+1)
    Y = [y for y in Y if (math.isnan(y) == False and y>=0)]
    X=[]
    print("Following data is being taken")
    print("--Date ---|----- Data--") 
    for i in range(0,len(Y)):
       if(i>=(len(Y)-min(daysInMonth(m,y)-filled_days,len(Y)))):
           X.append(Y[i])
           if(i<9):
               s = " | " 
           else:
               s = "| "  
           print(str(dates[i])+ " " +getMonth(ms)+ s + str(round(float(Y[i]),3)) )
    if (y*100+m) in df.columns: 
        Z = df[y*100+m].values.tolist()
        dates = []
        for i in range (0,daysInMonth(m,y)):
            if(math.isnan(Z[i]) == False and Z[i]>=0):
                dates.append(i+1)
        Z = [z for z in Z if (math.isnan(z) == False and z>=0)]
        for i in range(0,filled_days):
           if(i<9):
               s = " | " 
           else:
               s = "| "  
           print(str(dates[i])+ " " +getMonth(m)+ s + str(round(float(Z[i]),3)) ) 
        for i in range(0,filled_days):
            X.append(Z[i])    
    return (X,ms,ys)


#----------------Generates control limits for a particular data----------------#
def limits_generator(data, coef):
    mean = np.mean(data)
    std_dev = np.std(data,ddof = 1)
    if(coef == 3):
        limits = []

        for i in range(0,7):
            limits.append(mean-3*std_dev+i*std_dev)
        return limits
    elif(coef == 2):
        limits = []
        for i in range(0,5):
            limits.append(mean-2*std_dev+i*std_dev)

        return limits
    
#----------------------------Check data for linear trend-----------------------#
def linearity(data):
    Y=np.zeros(len(data))
    x= np.zeros(len(data))
    for i in range(0,len(data)):
        Y[i]= i
        x[i]= data[i]
    slope, intercept, r_value, p_value, std_err = stats.linregress(x,Y)
    if(r_value**2>0.33 and p_value<0.01):
        return 0
    else:
        return 1

#----------------------------Check data for normality--------------------------#
def normality(X):
    data = np.zeros(len(X))
    mean = np.mean(X)
    std_dev = np.std(X,ddof = 1)
    for i in range(0, len(X)):
        data[i] = (X[i]-mean)/std_dev
    x = stats.kstest(data, 'norm')
    if(x.pvalue < 0.05):
        return 0
    else:
        return 1    

#----------------------------Comparison to previous years--------------------------#
def prev_data(month,year):
    df = pd.read_excel('Control_Chart_Training.xlsx', sheet_name='Sheet1')
    same_month_last_years= []
    for col in df.columns: 
        x = int(col)
        if(x%100 == month and x//100 != year):
            same_month_last_years.append(x)
    if(len(same_month_last_years) == 0):
        print("As data for " + getMonth(month)+ " doesn't exist, we will not be comparing data points with past years.")
        return ([],-1,-1)
    else:
        print("Please choose an year from below for which you would like to compare the month's data in case of an exception:")
        for i in range(len(same_month_last_years)):
            print(same_month_last_years[i]//100)
        s = int(input("Your response in YYYY format:"))
        year_month = (s*100 + month)
        X = df[year_month].values.tolist()
        dates = []
        for i in range (0,daysInMonth(month,s)):
            if(math.isnan(X[i]) == False and X[i]>=0):
                dates.append(i+1)
        
        X = [x for x in X if (math.isnan(x) == False and x>=0 )]
        print("Data from " + getMonth(month) + " for year "+ str(s) + " is listed below.")
        print("--Date ---|----- Data--")    
        for i in range(0,len(X)):
            if(i<9):
                z = " | " 
            else:
                z = "| "  
            print(str(dates[i])+ " " +getMonth(month)+ z + str(round(float(X[i]),3)) )
        return (X,month,s)


def report_data(date,m,y):
    df = pd.read_excel('Control_Chart_Training.xlsx', sheet_name='Sheet1')
    if (y*100+m) in df.columns:
        X = df[y*100+m].values.tolist()
    else:
        X=[None]*date
    cond_list=[]
    data_list=X[0:(date-1)]
    for i in range(0,date-1):
        if(X[i] is None or math.isnan(X[i])==True):
            cond_list.append(-1)
            continue
        elif(X[i]<0):
            cond_list.append(1)
            continue
        else:
            cond_list.append(0)
    return (cond_list,data_list)


#--------------------------Tests for various rules-----------------------------------#
def test_near(X,limits,data_point,which_limit):
    count = 0 
    s = len(limits)
    a=1
    b=0
    if(which_limit=="UCL"):
        a=s-2
        b=s-1
    if(data_point>=limits[a] and data_point<=limits[b]):
        count = count+1 
    else:
        return 0
    for j in range(0,2):
        if(X[j]>=limits[a] and X[j]<=limits[b]):
            count+= 1
    if(count >= 2):
        return 1
    else:
        return 0

def test_bUC_bLC(X,limits,data_point,type_of_test):
    a=1
    b=2
    if(type_of_test=="bUC"):
        a=4
        b=5        
    count = 0 
    if(data_point>=limits[a] and data_point<=limits[b]):
        count = count+1 
    else:
        return 0
    for j in range(0,4):
        if(X[j]>=limits[a] and X[j]<=limits[b]):
            count+= 1
    if(count >= 4):
        return 1
    else:
        return 0

def test_trend_ud(X,limits,data_point,ud):
    s = len(limits)
    c = 0
    if(s == 7):
        c = 3
    else:
        c = 2
    count = 0
    
    a=c+1
    b=c
    if(ud=="Down"):
        a=c
        b=c-1
    if(data_point <= limits[a] and data_point >= limits[b]):
        count +=1
    else:
        return 0 
    for j in range(0,6):
        if(X[j]<=limits[a]and X[j]>=limits[b]):
            count+= 1
    if(count == 7):
        return 1
    else:
        return 0

#-----------------------------Check if data for date exists----------------------------#
def already(date,month,year):
    df = pd.read_excel('Control_Chart_Training.xlsx', sheet_name='Sheet1',nrows=50)
    X=[]
    year_month=100*year+month    
    for col in df.columns: 
        if(int(col)==(year_month)):
                X = df[year_month].values.tolist()
    m=0
    for i in range(0,len(X)):
        if(math.isnan(X[i]) == False):
            m=i+1
    if(date<=m):
        return 1
    return 0        






#-----------------------------Main function----------------------------#

#rule selection
rule_var=0
rule = rule_selection()
while(rule_var==0):
    rule = int(input())
    if(rule==0 or rule==1 or rule==2 or rule==3 ):
        rule_var=1
    else:
        print("Please choose from 0, 1, 2 and 3 only")

#risk coeffecient delection
coef_var=0
print("Enter risk coefficient to be used for computing control limits, risk coefficient must be either 2 or 3: ")
while(coef_var==0):   #rule selection
    coef = int(input())
    if(coef==2 or coef==3 ):
        coef_var=1
    else:
        print("Please choose from 2 and 3 only")

#entering data
s = "continue"
while(s=="continue"):
    input_var=0
    print("Input Month(MM) Year(YYYY) as MM YYYY for which data is being entered. If you want to exit, enter EXIT:")
    while(input_var==0):
        d=input()
        temp=re.findall(r'\d+',d)
        if(len(temp)==2):
            [month,year]= list(map(int, temp))
            if(month<=0 or month>12):
                print("Input is in wrong format.")
            else:
                input_var=1            
        elif(d=="EXIT"):
            input_var=1
        else:
            print("Input is in wrong format.")
    if(d=="EXIT"):
        break
        
    filled_days=reader(month,year)
     
    if(filled_days==daysInMonth(month,year)):
        print("All the data for this month is already available. Please choose some other month")
        continue
    elif(filled_days>=0 and filled_days<daysInMonth(month,year)):
        print("Data for "+str(filled_days)+" days in this month are available. Choose month from which remaining data should be taken to complete the rolling window")
        data_base, m1, y1=rolling_dataset(month,year,filled_days)
        
        #generating limits
        limits_prev = limits_generator(data_base, coef)
        print("LCL is "+str(round(float(limits_prev[0]),3)))   
        print("CL is "+str(round(float(limits_prev[(len(limits_prev)-1)//2]),3)))     
        print("UCL is "+str(round(float(limits_prev[len(limits_prev)-1]),3)))
        
        #linearity/normality tests
        check_linearity = linearity(data_base) 
        check_normality = normality(data_base)
        if(check_linearity == 0):
            print("Data from entered month and year has a linear trend.")
            break
        else:
            print("Data from entered month and year doesn't have a linear trend and is suitable for building control chart.")
        if(check_normality == 0):
            print("Data from entered month and year is not normally distributed.")
            break
        else:
            print("Data from entered year is normally distributed and is suitable for building control chart.")
            
            #previous year comparison
            limits_compare = []
            d_compare,m_compare,y_compare = prev_data(month,year)
            if(m_compare != -1):
                limits_compare = limits_generator(d_compare,coef)
                check_linearity = linearity(d_compare) 
                check_normality = normality(d_compare)
                if(check_linearity == 0):
                    print("Data from " + str(y_compare) + " has a linear trend.")
                    break
                else:
                    print("Data from " + str(y_compare) + " doesn't have a linear trend and is suitable for building control chart.")
                if(check_normality == 0):
                    print("Data from " + str(y_compare) +" is not normally distributed.")
                    break
                else:
                    print("Data from " + str(y_compare) +" is normally distributed and is suitable for building control chart.")
                    
             
            print("Enter date in format DD:")
            date=int(input())
            date_var=0
            while(date_var==0):
                if(date<=0 or date>daysInMonth(month,year)):
                    print("Wrong format, enter date again")
                elif(already(date,month,year)==1):
                    print("Data till this date is already available")
                    print("Enter date in format DD:")
                    date=int(input())
                else:
                    date_var=1            
            (cond_list,data_list)=report_data(date,month,year)
            points_in_month=0
            data_month = []
            for i in range(0,len(data_base)):
                data_month.insert(0,data_base[i])
            
            for date_counter in range(date,daysInMonth(month,year)+1):
                data_point = input("Enter consumption for " + str(date_counter)+ " "+ getMonth(month)+", " + str(year)+" or press E to exit: ")
                if(data_point == 'E'):
                    break
                else:
                   data_point = float(data_point)
                   points_in_month+=1    
                   neg=0
                   limits = limits_generator(data_month,coef)
                   size_limits = len(limits)
                    #---------------------Test for out of UCL and LCL-------------------_#
                   if(data_point > limits[size_limits-1]):
                       flag = 1
                       print("Consumption is high in comparison to previous 30/31 days.")
                       if(y_compare != -1):
                           if(data_point < limits_compare[0] ):
                               print("Consumption is high in comparison to past year's same month data.")
                       decision = input("Do you wish to include this data point (Y/N):")
                       if(decision == "Y"):
                           cond_list.append(2)
                           data_month.pop()
                           data_month.insert(0,data_point)
                       if(decision == "N"):
                           cond_list.append(1)
                           neg=1
                   elif(data_point < limits[0]):
                       flag = 1
                       print("Consumption is low in comparison to previous 30/31 days.")
                       if(y_compare != -1):
                           if(data_point < limits_compare[0] ):
                               print("Consumption is low in comparison to past year's same month data.")
                       decision = input("Do you wish to include this data point (Y/N):")
                       if(decision == "Y"):
                           cond_list.append(2)
                           data_month.pop()
                           data_month.insert(0,data_point)
                       if(decision == "N"):
                           cond_list.append(1)
                           neg=1
                   else:
                       flag = 0
                       nearUCL = test_near(data_month,limits,data_point,"UCL")
                       if(nearUCL == 1):
                           print("Rule 2 is violated,i.e., atleast 2 out of 3 consecutive points are in zone near upper control limit.")
                           decision = input("Do you wish to include most recent data point (Y/N):")
                           flag = 1
                           if(decision == "Y"):
                               cond_list.append(2)
                               data_month.pop()
                               data_month.insert(0,data_point)
                           if(decision == "N"):
                               cond_list.append(1)   
                               neg=1
                       nearLCL = test_near(data_month,limits,data_point,"LCL")
                       if(nearLCL == 1):
                           flag = 1
                           print("Rule 2 is violated,i.e.,atleast 2 out of 3 consecutive points are in zone near Lower Control Limit.")
                           decision = input("Do you wish to include this data (Y/N):")
                           if(decision == "Y"):
                               cond_list.append(2)
                               data_month.pop()
                               data_month.insert(0,data_point)
                           if(decision == "N"):
                               cond_list.append(1)
                               neg=1
                               
                       if(len(limits) == 7 and (rule == '1' or rule == '3')):
                           checkUC = test_bUC_bLC(data_month,limits,data_point,"bUC")
                           if(checkUC == 1):
                               flag = 1
                               print("4 out of 5 consecutive points are in Middle Zone.")
                               decision = input("Do you wish to include the most recent data point (Y/N):")
                               if(decision == "Y"):
                                   cond_list.append(2)
                                   data_month.pop()
                                   data_month.insert(0,data_point)
                               if(decision == "N"):
                                   cond_list.append(1)
                                   neg=1
                           checkLC = test_bUC_bLC(data_month,limits,data_point,"bLC")
                           if(checkLC == 1):
                               flag = 1
                               print("4 out of 5 consecutive points are in Middle Zone.")
                               decision = input("Do you wish to include the most recent data point (Y/N):")
                               if(decision == "Y"):
                                   cond_list.append(2)
                                   data_month.pop()
                                   data_month.insert(0,data_point)
                               if(decision == "N"):
                                   cond_list.append(1)
                                   neg=1
                               continue
                       if(rule == '2' or rule == '3'):
                           checkTrendUp = test_trend_ud(data_month,limits,data_point,"Up")
                           if(checkTrendUp == 1):
                               flag = 1
                               print("7 consecutive points are trending up in zone near Central Limit")
                               decision = input("Do you wish to include the most recent data point (Y/N):")
                               if(decision == "Y"):
                                   cond_list.append(2)
                                   data_month.pop()
                                   data_month.insert(0,data_point)
                               if(decision == "N"):
                                   cond_list.append(1)
                                   neg=1
                               continue
                           checkTrendDown = test_trend_ud(data_month,limits,data_point,"Down")
                           if(checkTrendDown == 1):
                               flag = 1
                               print("7 consecutive points are trending up in zone near Central Limit")
                               decision = input("Do you wish to include the most recent data point (Y/N):")
                               if(decision == "Y"):
                                   cond_list.append(2)
                                   data_month.pop()
                                   data_month.insert(0,data_point)
                               if(decision == "N"):
                                   cond_list.append(1)
                                   neg=1
                               continue
                   if(flag == 0):
                       cond_list.append(0)
                       data_month.pop()
                       data_month.insert(0,data_point)
                       print("The data entered is in control.")
                   if(neg!=0):
                       data_list.append(-data_point)
                   else:    
                       data_list.append(data_point)    
                   limits = limits_generator(data_month,coef)
            

            check = input("Do you want a weekly report? Y/N:")
            week_var=0
            while(week_var==0):
                if(check=='Y'):
                    report_generator(data_list,cond_list,limits,month,year,"week")
                    week_var=1
                elif(check=='N'):
                    week_var=1
                else:
                    print("Please choose from 'Y' or 'N' only")
                    
            check = input("Do you want a monthly report? Y/N:")
            month_var=0
            while(month_var==0):
                if(check=='Y'):
                    report_generator(data_list,cond_list,limits,month,year,"month")
                    month_var=1
                elif(check=='N'):
                    month_var=1
                else:
                    print("Please choose from 'Y' or 'N' only")
                     
            check = input("Do you want to export this data to excel? Y/N:")
            export_var=0
            while(export_var==0):
                if(check=='Y'):
                    list_to_excel(data_list,month,year)
                    export_var=1
                elif(check=='N'):
                    export_var=1
                else:
                    print("Please choose from 'Y' or 'N' only")             
                 
            if(points_in_month!=(daysInMonth(month,year)-date+1)):
                s="exit"
            else:
                check = input("We have completed for this month. Do you wish to proceed? Y/N:")
                proceed_var=0
                while(proceed_var==0):
                    if(check=='Y'):
                        proceed_var=1
                    elif(check=='N'):
                        s="exit"
                        proceed_var=1
                    else:
                        print("Please choose from 'Y' or 'N' only")